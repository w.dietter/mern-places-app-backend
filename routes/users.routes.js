const express = require('express');
const router = express.Router();
const { check } = require('express-validator');

const usersController = require('../controllers/users.controller');

/* GET -> /api/users */
router.get('/', usersController.getAllUsers);

/* POST -> /api/users/signup */
router.post(
  '/signup',
  [
    check('name')
      .not()
      .isEmpty(),
    check('email').isEmail(),
    check('password')
      .not()
      .isEmpty()
      .isLength({ min: 6 })
  ],
  usersController.signUp
);

/* POST -> /api/users/login */
router.post('/login', usersController.login);

module.exports = router;
