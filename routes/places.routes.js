const express = require('express');
const router = express.Router();
const { check } = require('express-validator');

const placesController = require('../controllers/places.controller');

/* ************************* Routes ********************* */

/* GET -> /api/places */
router.get('/', placesController.getAllPlaces);

/* GET -> /api/places/user/:uid */
router.get('/user/:uid', placesController.getPlacesByUserId);

/* GET -> /api/places/:pid */
router.get('/:pid', placesController.getPlaceById);
module.exports = router;

/* POST -> /api/places/ */
router.post(
  '/',
  [
    check('title')
      .not()
      .isEmpty(),
    check('description').isLength({ min: 5 }),
    check('address')
      .not()
      .isEmpty(),
    check('creator')
      .not()
      .isEmpty()
  ],
  placesController.createPlace
);

/* PATCH -> /api/places/:pid */
router.patch(
  '/:pid',
  [
    check('title')
      .not()
      .isEmpty(),
    check('description').isLength({ min: 5 })
  ],
  placesController.updatePlace
);

/* DELETE -> /api/places/:pid */
router.delete('/:pid', placesController.deletePlace);

module.exports = router;
