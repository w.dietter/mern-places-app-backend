const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 6
  },
  image: {
    type: String,
    required: true
  },
  places: [
    {
      type: mongoose.Types.ObjectId,
      ref: 'Place',
      required: true
    }
  ]
});

UserSchema.plugin(uniqueValidator);

if (!UserSchema.options.toObject) {
  UserSchema.options.toObject = {};
  UserSchema.options.toObject.transform = (doc, ret, options) => {
    delete ret.password;
    return ret;
  };
}

module.exports = mongoose.model('User', UserSchema);
