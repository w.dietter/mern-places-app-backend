const mongoose = require("mongoose");
const { validationResult } = require("express-validator");

const HttpError = require("../models/http-error.model");
const PlaceModel = require("../models/Place.model");
const UserModel = require("../models/User.model");

/* GET -> /api/places *********************************************************/

const getAllPlaces = async (req, res, next) => {
  try {
    const places = await PlaceModel.find();
    return res.json({
      places: places.map(place => place.toObject({ getters: true }))
    });
  } catch (error) {
    return next(error || new HttpError("Something went wrong", 500));
  }
};

/* GET -> /api/places/user/:uid ***********************************************/

const getPlaceById = async (req, res, next) => {
  const pid = req.params.pid;
  let place;

  try {
    place = await PlaceModel.findById(pid);
  } catch (error) {
    error = new HttpError("Something went wrong", 500);
    return next(error);
  }

  if (!place) {
    /* 1st option throw a new Error that will be catched. */
    return next(
      new HttpError("Could not find a place for the provided id.", 404)
    );
  }

  return res.status(200).json({ place: place.toObject({ getters: true }) });
};

/* GET -> /api/places/:pid ****************************************************/

const getPlacesByUserId = async (req, res, next) => {
  const userId = req.params.uid;

  let userWithPlaces;
  try {
    userWithPlaces = await UserModel.findById(userId).populate("places");
    if (userWithPlaces.places.length === 0 || !userWithPlaces) {
      return next(
        new HttpError("Could not find places for the given user id.", 404)
      );
    }

    return res.json({
      places: userWithPlaces.places.map(place =>
        place.toObject({ getters: true })
      )
    });
  } catch (error) {
    return next(error || new HttpError("Something went wrong", 500));
  }
};

/* POST -> /api/places ********************************************************/

const createPlace = async (req, res, next) => {
  /* VALIDATION */
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors });
  }

  const { title, description, coordinates, address, creator } = req.body;

  const createdPlace = new PlaceModel({
    title,
    description,
    address,
    // location: { lat: coordinates.lat, lng: coordinates.lng },
    location: { lat: 11, lng: 22 },
    creator,
    image: "test.image"
  });

  let user;
  try {
    user = await UserModel.findById(creator);
  } catch (error) {
    const err = new HttpError("Creating place failed, please try again", 500);
    return next(err);
  }

  if (!user) {
    const error = new HttpError("Could not find user for provided id", 500);
    return next(error);
  }

  try {
    //Transactions: Perfom multiple operations, use sessions.
    const session = await mongoose.startSession();

    session.startTransaction();
    await createdPlace.save({ session: session });
    //Only adds the place id, no the entire document.
    user.places.push(createdPlace);

    await user.save({ session: session });

    //Commit the transtaction
    await session.commitTransaction();
  } catch (err) {
    error = new HttpError("Creating place failed.", 500);
    return next(err || error);
  }

  return res.status(201).json({
    place: createdPlace
  });
};

/* PATCH -> /api/places/:pid **************************************************/

const updatePlace = async (req, res, next) => {
  /* VALIDATION */
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors });
  }

  const { title, description } = req.body;
  const { pid } = req.params;

  let place;
  try {
    place = await PlaceModel.findById(pid);
    place.title = title;
    place.description = description;

    await place.save();

    res.status(200).json({ place: place.toObject({ getters: true }) });
  } catch (error) {
    return next(error || new HttpError("Something went wrong.", 500));
  }
};

/* DELETE -> /api/places/:pid *********************************************** */

const deletePlace = async (req, res, next) => {
  const { pid } = req.params;

  let place;
  try {
    place = await PlaceModel.findById(pid).populate("creator");
  } catch (error) {
    return next(error || new HttpError("Something went wrong."));
  }

  if (!place) {
    return res.status(422).json({
      message: "Place not found."
    });
  }

  try {
    //Transaction
    const session = await mongoose.startSession();
    session.startTransaction();

    await place.remove({ session: session });

    // populate gives the full user linked to the place,
    // is not necesary to search the user from outside of the populate method.

    place.creator.places.pull(place);
    await place.creator.save({ session: session });
    await session.commitTransaction();
    res.status(200).json({
      message: "Place deleted.",
      place: place.toObject({
        getters: true
      })
    });
  } catch (error) {
    return next(err || new HttpError("Something went wrong.", 500));
  }
};

module.exports = {
  getAllPlaces,
  getPlaceById,
  getPlacesByUserId,
  createPlace,
  updatePlace,
  deletePlace
};
