const HttpError = require("../models/http-error.model");
const { validationResult } = require("express-validator");

const UserModel = require("../models/User.model");

/* GET -> /api/users **********************************************************/

const getAllUsers = async (req, res, next) => {
  try {
    const users = await UserModel.find({}, "-password");
    return res.status(200).json({
      users: users.map(user => {
        return user.toObject({
          getters: true
        });
      })
    });
  } catch (error) {
    return next(error || new HttpError("Something went wrong", 500));
  }
};

/* POST -> /api/users/signup **************************************************/

const signUp = async (req, res, next) => {
  /* VALIDATION */
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors });
  }

  const { name, email, password } = req.body;

  let existingUser;
  try {
    existingUser = await UserModel.findOne({ email });
    if (existingUser) {
      return next(new HttpError("User allready exists.", 422));
    }

    const createdUser = new UserModel({
      name,
      email,
      image: "test",
      password,
      places: []
    });

    await createdUser.save();

    return res.status(201).json({
      message: "User created.",
      user: createdUser.toObject({
        getters: true,
        transform: true
      })
    });
  } catch (error) {
    return next(error || new HttpError("Something went wrong."));
  }
};

/* POST -> /api/users/login ***************************************************/

const login = async (req, res, next) => {
  const { email, password } = req.body;

  let identifiedUser;
  try {
    identifiedUser = await UserModel.findOne({ email });

    if (!identifiedUser || identifiedUser.password !== password) {
      return next(new HttpError("Bad credentials", 401));
    }

    res.status(200).json({
      message: "Logged in.",
      user: identifiedUser.toObject({ getters: true })
    });
  } catch (error) {
    return next(error || new HttpError("Something went wrong.", 500));
  }
};

module.exports = {
  getAllUsers,
  signUp,
  login
};
