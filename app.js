const express = require("express");
const HttpError = require("./models/http-error.model");
const mongoose = require("mongoose");
const chalk = require("chalk");

const placesRoutes = require("./routes/places.routes");
const usersRoutes = require("./routes/users.routes");

/* CREATE EXPRESS APP */
const app = express();
app.use(express.json());

/*CORS*/
/*app.use((req, res, next) => {
  //Add headers to the response to prevent cors error.
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE');
});
*/

/* MIDDLEWARE */
app.use((req, res, next) => {
  console.log(
    chalk.bgWhiteBright.black(`.....${req.method} -> ${req.originalUrl}.....`)
  );
  if (req.body !== {}) {
    console.log(chalk.green("body"));
    console.log(req.body);
  }
  if (req.params) {
    console.log(chalk.blue("params"));
    console.log(req.params);
  }
  next();
});

/* ROUTES */
app.use("/api/places", placesRoutes);
app.use("/api/users", usersRoutes);

/* route not found middleware for requests that didnt match any route */
app.use((req, res, next) => {
  const error = new HttpError("Could not found this route", 404);
  return next(error);
});

/* error handler middleware */
app.use((error, req, res, next) => {
  if (res.headerSent) {
    /* forward the error because response already been sent. */
    return next(error);
  }
  console.log(chalk.bgRed(error && error.message + " " + error.code));
  /* error.code and error.message are set on the HttpError constructor. */
  res.status(error.code || 500).json({
    message: error.message || "Something went wrong."
  });
});

/* MONGO DB CONNECTION */
mongoose
  .connect(
    "mongodb+srv://admin:1234@cluster0-x8ofn.mongodb.net/places?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    }
  )
  .then(() => {
    /* START SERVER */
    app.listen(5000, () => {
      console.log(
        `${chalk.whiteBright(
          new Date().toLocaleString()
        )} ${chalk.bold.bgBlue.whiteBright(
          "server started on port 5000"
        )} -> ${chalk.bold.bgGreen.whiteBright("Connected To MongoDB")}
        `
      );
    });
  })
  .catch(error => {
    console.log("Can't connect with MongoDb");
    console.log(error);
  });
